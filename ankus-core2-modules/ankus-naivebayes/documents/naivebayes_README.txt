확률 기반 데이터 분류 모델

수행 방법:
hadoop jar ankus-core2-naivebayes-1.1.0.jar \
NaiveBayes \
-input [입력 파일 또는 입력 폴더] \
-output [출력 폴더 경로] \
-indexList [수치형 대상 인덱스 리스트] \
-nominalIndexList [범주형 대상 인덱스 리스트] \
-classIndex [클래스 레이블 인덱스] \
-delimiter [컬럼 구분자] \
-finalResultGen true

입력 데이터의 속성이 수치형으로만 존재할 경우 nominalIndexList를 생략함.
입력 데이터의 속성이 범주형으로만 존재할 경우 indexList를 생략함.
-finalResultGen false로 설정, 혹은 사용하지 않을 경우  모델만 생성

-finalResultGen true로 설정시 
bayes_rules, classifying_result/part-m-00000,  validation가 생성됨.
bayes_rules  : 모델 파일
classifying_result/part-m-00000 : 분류 결과 파일
validation : 성능 검증 결과 파일

수행 예제:

-모델 생성:
hadoop jar ankus-core2-naivebayes-1.1.0.jar \
NaiveBayes  \
-input /data/naivebayes.csv \
-output /result/naive_bayes_model \
-indexList 0,1,2,3 \
-classIndex 4 \
-finalResultGen true \
-delimiter ,

*입력 예제
........
5.1,2.5,3,1.1,versicolor
5.7,2.8,4.1,1.3,versicolor
6.3,3.3,6,2.5,virginica
5.8,2.7,5.1,1.9,virginica
7.1,3,5.9,2.1,virginica
6.3,2.9,5.6,1.8,virginica
6.5,3,5.8,2.2,virginica
7.6,3,6.6,2.1,virginica
4.9,2.5,4.5,1.7,virginica
7.3,2.9,6.3,1.8,virginica
6.7,2.5,5.8,1.8,virginica
7.2,3.6,6.1,2.5,virginica
6.5,3.2,5.1,2,virginica
6.4,2.7,5.3,1.9,virginica
6.8,3,5.5,2.1,virginica
.........
*출력 예제
**/result/naive_bayes_model/bayes_rules  : 모델 파일
# AttrIndex(or Class),Type,Value(Category or Avg/StdDev),ValueCount,ClassType,ClassCount
0,numeric,5.006,0.3489469873777273,50,setosa,50
1,numeric,3.4179999999999997,0.3771949098278028,50,setosa,50
2,numeric,1.464,0.1717672844286691,50,setosa,50
3,numeric,0.244,0.10613199329137288,50,setosa,50
0,numeric,5.936,0.5109833656783921,50,versicolor,50
1,numeric,2.7700000000000005,0.3106444913401753,50,versicolor,50
2,numeric,4.26,0.46518813398452424,50,versicolor,50
3,numeric,1.3259999999999996,0.19576516544063882,50,versicolor,50
0,numeric,6.587999999999998,0.6294886813915159,50,virginica,50
1,numeric,2.9739999999999993,0.31925538366644257,50,virginica,50
2,numeric,5.551999999999998,0.5463478745268635,50,virginica,50
3,numeric,2.025999999999999,0.271889683511538,50,virginica,50
class,virginica,50,150
class,setosa,50,150
class,versicolor,50,150
**/result/naive_bayes_model/classifying_result/part-m-00000 : 분류 결과 파일
..............
5.7,2.6,3.5,1,versicolor,versicolor
5.5,2.4,3.8,1.1,versicolor,versicolor
5.5,2.4,3.7,1,versicolor,versicolor
5.8,2.7,3.9,1.2,versicolor,versicolor
6,2.7,5.1,1.6,versicolor,versicolor
5.4,3,4.5,1.5,versicolor,versicolor
6,3.4,4.5,1.6,versicolor,versicolor
6.7,3.1,4.7,1.5,versicolor,versicolor
6.3,2.3,4.4,1.3,versicolor,versicolor
................
**/result/naive_bayes_model/validation : 성능 검증 결과 파일
# Total Summary
Total Instances: 150
Correctly Classified Instances: 144(96.00%)
Incorrectly Classified Instances: 6(4.00%)

# Confusion Matrix
(Classified as)	setosa	versicolor	virginica	|	total	
setosa	50	0	0	|	#50
versicolor	0	47	3	|	#50
virginica	0	3	47	|	#50
total	50	50	50

# Detailed Accuracy
Class	TP_Rate	FP_Rate	Precision	Recall	F-Measure
setosa	1.000	0.000	1.000	1.000	1.000
versicolor	0.940	0.030	0.940	0.940	0.940
virginica	0.940	0.030	0.940	0.940	0.940
Weig.Avg.	0.960	0.020	0.960	0.960	0.960

-테스트: 
hadoop jar ankus-core2-naivebayes-1.1.0.jar \
NaiveBayes  \
-input /data/naivebayes.csv \
-modelPath /result/naive_bayes_model/bayes_rules \
-output /result/naive_bayes_test \
-indexList 0,1,2,3 \
-classIndex 4 \
-delimiter ,

**/result/naive_bayes_test/validation : 성능 검증 결과 파일
# Total Summary
Total Instances: 150
Correctly Classified Instances: 144(96.00%)
Incorrectly Classified Instances: 6(4.00%)

# Confusion Matrix
(Classified as)	setosa	versicolor	virginica	|	total	
setosa	50	0	0	|	#50
versicolor	0	47	3	|	#50
virginica	0	3	47	|	#50
total	50	50	50

# Detailed Accuracy
Class	TP_Rate	FP_Rate	Precision	Recall	F-Measure
setosa	1.000	0.000	1.000	1.000	1.000
versicolor	0.940	0.030	0.940	0.940	0.940
virginica	0.940	0.030	0.940	0.940	0.940
Weig.Avg.	0.960	0.020	0.960	0.960	0.960

**/result/naive_bayes_test/classifying_result/part-m-00000 : 분류 결과 파일
5,3.5,1.3,0.3,setosa,setosa
4.5,2.3,1.3,0.3,setosa,setosa
4.4,3.2,1.3,0.2,setosa,setosa
5,3.5,1.6,0.6,setosa,setosa
5.1,3.8,1.9,0.4,setosa,setosa
4.8,3,1.4,0.3,setosa,setosa
5.1,3.8,1.6,0.2,setosa,setosa
4.6,3.2,1.4,0.2,setosa,setosa
5.3,3.7,1.5,0.2,setosa,setosa
5,3.3,1.4,0.2,setosa,setosa
7,3.2,4.7,1.4,versicolor,versicolor
6.4,3.2,4.5,1.5,versicolor,versicolor
6.9,3.1,4.9,1.5,versicolor,virginica
5.5,2.3,4,1.3,versicolor,versicolor
6.5,2.8,4.6,1.5,versicolor,versicolor
5.7,2.8,4.5,1.3,versicolor,versicolor
6.3,3.3,4.7,1.6,versicolor,versicolor
4.9,2.4,3.3,1,versicolor,versicolor
6.6,2.9,4.6,1.3,versicolor,versicolor
5.2,2.7,3.9,1.4,versicolor,versicolor
