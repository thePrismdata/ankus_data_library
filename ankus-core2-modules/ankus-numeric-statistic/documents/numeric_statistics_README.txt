수치 데이터 통계 분석 모듈

수행 방법:
hadoop jar ankus-core2-numeric-statistic-1.1.0.jar \
NumericStatistics \
-input [입력 파일 또는 입력 폴더] \
-output [출력 폴더 경로] \
-delimiter [구분자] \
-indexList  [수치형 대상 인덱스 리스트] \
-tempDelete [임시 파일 삭제 옵션{true, false}]
임시 파일 경로
[출력 폴더 경로]_2_1_BlockPos
[출력 폴더 경로]_2_2_BlockPos
[출력 폴더 경로]_res1
[출력 폴더 경로]_splitStat

수행 예제:
hadoop jar ankus-core2-numeric-statistic-1.1.0.jar \
NumericStatistics \
-input /data/numeric_statistics.csv \
-output /result/numeric_statistic \
-delimiter , \
-indexList 0,1,2,3 \
-tempDelete false

*입력 파일:
....
7.1,3,5.9,2.1,virginica
6.3,2.9,5.6,1.8,virginica
6.5,3,5.8,2.2,virginica
7.6,3,6.6,2.1,virginica
4.9,2.5,4.5,1.7,virginica
7.3,2.9,6.3,1.8,virginica
6.7,2.5,5.8,1.8,virginica
7.2,3.6,6.1,2.5,virginica
6.5,3.2,5.1,2,virginica
6.4,2.7,5.3,1.9,virginica
6.8,3,5.5,2.1,virginica
5.7,2.5,5,2,virginica
5.8,2.8,5.1,2.4,virginica
6.4,3.2,5.3,2.3,virginica
6.5,3,5.5,1.8,virginica
7.7,3.8,6.7,2.2,virginica
....
**출력 파일:
**/result/numeric_statistic/result
# AttrIndex,Sum,Average,HarmonicAverage,GeographicAverage,Variance,StandardDeviation,Max,Min,DataCnt,1stQuartile,2ndQuartile,3rdQuartile
0,876.5,5.843,5.729,5.786,0.681,0.825,7.9,4.3,150,5.1,5.8,6.4
1,458.1,3.054,2.993,3.024,0.187,0.432,4.4,2.0,150,2.8,3.0,3.3
2,563.8,3.759,2.696,3.24,3.092,1.759,6.9,1.0,150,1.6,4.35,5.1
3,179.8,1.199,0.487,0.838,0.579,0.761,2.5,0.1,150,0.3,1.3,1.8