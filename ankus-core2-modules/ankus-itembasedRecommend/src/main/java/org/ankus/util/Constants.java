/*
 * Copyright (C) 2011 ankus (http://www.openankus.org).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.ankus.util;

/**
 * 알고리즘 수행 내부 상수.
 * @version 0.0.1
 * @date : 2013.07.15
 * @author Suhyun Jeon
 * @author Moonie Song
 */
public class Constants {
	/**
	 * <font face="verdana" color="green">아이템 기반 추천 알고리즘 드라이버 호출명</font>
	 */
    public static final String DRIVER_RECOMMENDATION = "Recommendation";
    /**
	 * <font face="verdana" color="green">추천 대상 아이템 지정 옵션 false일 경우 출력 폴더에 있는 내용을 객체로 읽은 후 정렬 하여 파일로 병합</font>
	 */
    public static final String RECOMJOB_ITEM_DEFINED = "false";
    /**
	 * <font face="verdana" color="green">사용자가 구매한 아이템 리스트 설정 인자</font>
	 */
    public static final String RECOMJOB_USERS_VIEWED_INFOS = "user_viwed_list";
    /**
   	 * <font face="verdana" color="green">파일 엔코딩 설정 인자</font>
   	 */
    public static final String UTF8 = "UTF-8";
}
