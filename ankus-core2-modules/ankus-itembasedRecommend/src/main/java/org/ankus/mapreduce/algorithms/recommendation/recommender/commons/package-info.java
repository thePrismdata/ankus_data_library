/**
 * <font face="verdana">
 * 추천 결과를 생성하는 패키지
 * </font>
 * @author HongJoong.Shin
 * @date 2017.09.29
 */
package org.ankus.mapreduce.algorithms.recommendation.recommender.commons;