/**
 * <font face="verdana">
 * 아이템 기반 추천 알고리즘 실행 패키지
 * </font>
 * @author Moonie Song
 * @date : 2013.07.02
 */
package org.ankus.mapreduce.algorithms.recommendation.recommender.itembased;