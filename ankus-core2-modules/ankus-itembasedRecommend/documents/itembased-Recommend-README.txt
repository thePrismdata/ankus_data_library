아이템간 유사도 모델을 사용한 아이템 추천 

수행 방법:
hadoop jar ankus-core2-itembasedRecommend-1.1.0.jar \
Recommendation \
-input [입력 파일 또는 입력 폴더] \
-output [출력 폴더 경로] \
-delimiter [속성 구분자(movielens 데이터의 경우 ::로 구분함] \
-uidIndex [사용자 컬럼 위치] \
-iidIndex [아이템 컬럼 위치] \
-ratingIndex [평점 컬럼 위치] \
-similPath [유사도 모델 경로] \
-similDelimiter [유사도의 아이템 구분자] \
-similThreshold [아이템간 유사도의 하한값] \
-targetUID [추천해줄 사용자 아이디] \
-recomCnt [추천해 줄 아이템의 갯수]

similThreshold : 0에서 1사이의 실수(아이템간 유사도 모델 생성에 사용한 유사도 계산 방법에 따름)

수행 예제:
hadoop jar ankus-core2-itembasedRecommend-1.1.0.jar Recommendation \
-input /data/itembased-Recommend.txt  \
-output /result/item_recommend \
-delimiter :: \
-uidIndex 0 \
-iidIndex 1 \
-ratingIndex 2 \
-similPath /resut/item_sim_result \
-similDelimiter :: \
-similThreshold 0.6 \
-targetUID U9 \
-recomCnt 20 

*입력 예제
.....
U5329::3524::4::961005226
U5329::3526::5::961011305
U5329::3528::4::961007575
U5329::2580::4::960916782
U5329::318::5::961002454
U5329::3535::4::968790599
U5329::3538::5::987486696
U5329::324::4::961456452
U5329::2598::3::960916999
U5329::2599::4::960916545
U5329::3543::5::960916528
U5329::3549::4::960837585
U5329::1946::5::960838364
U5329::11::4::961008571
U5329::337::5::960839374
U5329::17::5::960838561
U5329::3551::5::1004073143
U5329::3556::3::987486791
.....
*모델 예제
**/item_sim_result/part-r-00000
1000::1183::4.123
1000::1193::6.708
1000::1196::8.0
1000::1197::7.0
1000::1198::7.937
1000::1200::7.141
1000::1208::6.324
1000::1210::8.124
1000::1213::8.485
1000::1214::7.549
1000::1215::7.280
1000::1219::5.916
1000::1221::8.0
1000::1222::7.141
1000::1240::7.0

*출력 예제
**/resut/item_recommend/recomResult
2197::5.0::1
245::5.0::1
807::5.0::1
2740::4.667::3
3817::4.667::9
2079::4.571::7
3025::4.556::9
1117::4.5::4
1743::4.5::2
1872::4.5::2
2627::4.5::8
2839::4.5::2
2994::4.5::4
3344::4.5::8
754::4.5::2
632::4.429::7
283::4.417::12
3488::4.417::12
503::4.417::12
2998::4.4::10