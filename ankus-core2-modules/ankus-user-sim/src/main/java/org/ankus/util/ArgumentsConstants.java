/*
 * Copyright (C) 2011 ankus (http://www.openankus.org).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.ankus.util;

/**
 * 알고리즘 실행 파라미터 정의.
 * @desc
 *      Collected constants of parameter by user
 * @version 0.0.1
 * @date : 2013.08.23
 * @author Suhyun Jeon
 * @author Moonie Song
 */
public class ArgumentsConstants {

    // common
    public static final String INPUT_PATH = "-input";
    public static final String OUTPUT_PATH = "-output";
    public static final String DELIMITER = "-delimiter";
    public static final String SUB_DELIMITER = "-subDelimiter";
    public static final String TEMP_DELETE = "-tempDelete";
    public static final String ALGORITHM_OPTION = "-algorithmOption";       // cf-sim, contents based sim 
      
    // for recomendation
    public static final String COMMON_COUNT = "-commonCount";
    public static final String UID_INDEX = "-uidIndex";
    public static final String IID_INDEX = "-iidIndex";
    public static final String RATING_INDEX = "-ratingIndex";
    public static final String BASED_TYPE = "-basedType";
    public static final String TARGET_ID = "-targetID";
    
}
