확신도 기반 합.
 
수행 방법:
hadoop jar ankus-core2-certaintyfactorsum-1.1.0.jar \
CertaintyFactorSUM \
-input [입력 파일 또는 입력 폴더] \
-output [출력 데이터 폴더 경로] \
-delimiter [구분자] \
-indexList [확신도가 기술된 인덱스] \
-cfsumMax [0 이상의 실수 : 생략 가능]

수행 예제:
hadoop jar ankus-core2-certaintyfactorsum-1.1.0.jar \
CertaintyFactorSUM \
-input /data/certaintyfactorsum.csv \
-output /result/CertaintyFactorSum \
-delimiter , \
-indexList 1,2 
 
*입력 예제:
ExpertA,0.8,0.4
ExpertB,0.2,0.5
ExpertC,0.6,0.3
ExpertD,0.7,0.2
ExpertE,0.7,0.6
ExpertF,0.8,0.1

*출력 예제:
**/result/CertaintyFactorSum/part-r-00000 : 확신도의 합 출력
[변수번호, 확신도 합]
1,0.998848
2,0.9395199999999999