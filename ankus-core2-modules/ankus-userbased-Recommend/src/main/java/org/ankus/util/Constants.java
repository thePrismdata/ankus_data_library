/*
 * Copyright (C) 2011 ankus (http://www.openankus.org).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.ankus.util;

/**
 * 알고리즘 수행 인자를 상수로 정의함.
 * @desc
 *      Collected constants of parameter by user
 * @version 0.0.1
 * @date : 2013.08.23
 * @author Suhyun Jeon
 * @author Moonie Song
 */
public class Constants {
	// common
    public static final String INPUT_PATH = "-input";
    public static final String OUTPUT_PATH = "-output";
    public static final String DELIMITER = "-delimiter";
    public static final String SUB_DELIMITER = "-subDelimiter";

    // for recomendation
    public static final String UID_INDEX = "-uidIndex";
    public static final String IID_INDEX = "-iidIndex";
    public static final String RATING_INDEX = "-ratingIndex";
    public static final String BASED_TYPE = "-basedType";

    public static final String SIMILARITY_DELIMITER = "-similDelimiter";
    public static final String SIMILARITY_PATH = "-similPath";
    public static final String SIMILARITY_THRESHOLD = "-similThreshold";
    public static final String RECOMMENDATION_CNT = "-recomCnt";
    public static final String TARGET_UID = "-targetUID";
    public static final String TARGET_IID_LIST = "-targetIIDList";

    
    public static final String USER_INDEX = "-userIndex";
    public static final String ITEM_INDEX = "-itemIndex";
    public static final String THRESHOLD = "-threshold";
    public static final String SIMILARITY_DATA_INPUT = "-similarDataInput";
    public static final String RECOMMENDED_DATA_INPUT = "-recommendedDataInput";

    public static final String DRIVER_USER_BASE_RECOMMENDATION = "UserBased_Recommendation";
    
    public static final String RECOMJOB_ITEM_DEFINED = "false";
    public static final String RECOMJOB_SIMIL_USER_INFOS = "simil_user_list";
    public static final String RECOMJOB_USERS_VIEWED_INFOS = "user_viwed_list";
    public static final String MIDTERM_PROCESS_OUTPUT_DIR = "midterm.process.output.dir";

    public static final String UTF8 = "UTF-8";
    public static final String YYYYMMDDHHMMSS = "yyyyMMddHHmmss";
    
}
