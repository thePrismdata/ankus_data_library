수치/범주형 데이터에 대해 유클리드/멘허튼 거리 측정 방법을 이용하여 가까운 것끼리 묶어주는 군집 알고리즘입니다.

수행 방법:
hadoop jar ankus-core2-kmeans-1.1.0.jar \
KMeans \
-input [입력 파일 또는 입력 폴더] \
-output [출력 데이터 폴더 경로] \
-delimiter [속성 구분자]  \
-indexList [수치형 속성 목록] \
-clusterCnt [클러스터 개수]  \
-convergeRate [클러스터 중심의 이동 변화 수렴 값] \
-maxIteration [군집 반복 횟수] \
-finalResultGen [클러스터내의 데이터 갯수 출력 여부]

-clusterCnt : 정수 값으로 1이상 데이터 수 이하로 설정
-convergeRate : 0에서 1사이의 실수 값.
-maxIteration : 0에상 정수 값 
-finalResultGen true로 설정시

purity.csv : 군집의 데이터 분포
clustering_result.csv : 군집 결과
clustering_result/part-m-00000 : clustering_result.csv과 동일(엑셀 호환용으로 생성됨)

-finalResultGen false로 설정시
clustering_result_[군집 반복 횟수] /part-m-00000

수행 예제:
hadoop jar ankus-core2-kmeans-1.1.0.jar \
KMeans \
-input /data/kmeans.csv \
-output /result/kmeans_csv \
-delimiter , \
-indexList 0,1,2,3 \
-clusterCnt 4 \
-convergeRate 0 \
-maxIteration 100 \
-finalResultGen true

*입력 예제
........
5.1,2.5,3,1.1,versicolor
5.7,2.8,4.1,1.3,versicolor
6.3,3.3,6,2.5,virginica
5.8,2.7,5.1,1.9,virginica
7.1,3,5.9,2.1,virginica
6.3,2.9,5.6,1.8,virginica
6.5,3,5.8,2.2,virginica
7.6,3,6.6,2.1,virginica
4.9,2.5,4.5,1.7,virginica
7.3,2.9,6.3,1.8,virginica
6.7,2.5,5.8,1.8,virginica
7.2,3.6,6.1,2.5,virginica
6.5,3.2,5.1,2,virginica
6.4,2.7,5.3,1.9,virginica
6.8,3,5.5,2.1,virginica
.........
*출력 예제
**purity.csv : 군집의 데이터 분포
# Clustering Result - Purity
# Cluster Number,Assigned Data Count,Assigned Data Ratio
# Attr-4,frequency,ratio
0,27,0.18
1,23,0.153
2,61,0.407
3,39,0.26
**clustering_result.csv : 군집의 데이터 분포
...
6.9,3.2,5.7,2.3,3,0.279
5.6,2.8,4.9,2,2,0.816
7.7,2.8,6.7,2,3,1.329
6.3,2.7,4.9,1.8,2,0.755
6.7,3.3,5.7,2.1,3,0.275
7.2,3.2,6,1.8,3,0.53
6.2,2.8,4.8,1.8,2,0.638
6.1,3,4.9,1.8,2,0.714
6.4,2.8,5.6,2.1,3,0.546
7.2,3,5.8,1.6,3,0.582
7.4,2.8,6.1,1.9,3,0.739
7.9,3.8,6.4,2,3,1.445
6.4,2.8,5.6,2.2,3,0.563
....

