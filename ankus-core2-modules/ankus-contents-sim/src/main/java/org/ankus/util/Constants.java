/*
 * Copyright (C) 2011 ankus (http://www.openankus.org).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.ankus.util;

/**
 * 알고리즘 수행 내부 상수.
 * @version 0.0.1
 * @date : 2013.07.15
 * @author Suhyun Jeon
 * @author Moonie Song
 */
public class Constants {
    public static final String DRIVER_CONTENT_BASED_SIMILARITY = "ContentBasedSimilarity";

    public static final String CORR_DICE = "dice";
    public static final String CORR_JACCARD = "jaccard";
    public static final String CORR_MANHATTAN = "manhattan";

    public static final String RECOM_CB_NORMALSUM = "sum";
    public static final String RECOM_CB_AVGSUM = "avg";
    public static final String RECOM_CB_CFSUM = "cfsum";

    public static final String KEY_INDEX = "keyIndex";
    public static final String TARGET_INDEX = "indexList";
    public static final String DELIMITER = "delimiter";
    public static final String MIDTERM_PROCESS_OUTPUT_DIR = "midterm.process.output.dir";
    public static final String MIDTERM_PROCESS_OUTPUT_REMOVE_MODE = "midterm.process.output.remove.mode";
    public static final String YYYYMMDDHHMMSS = "yyyyMMddHHmmss";


}
