추천 알고리즘의 아이템간 유사도 모델을 생성 

수행 방법:
hadoop jar ankus-core2-contents-sim-1.1.0.jar \
ContentBasedSimilarity \
-input [입력 파일 또는 입력 폴더] \
-output [출력 데이터 폴더 경로] \
-delimiter [속성 구분자(movielens 데이터의 경우 ,로 구분함.] \
-subDelimiter  [콘텐츠 속성 구분자(movielens 데이터의 경우 |로 구분함] \
-indexList [속성 컬럼 위치] \
-keyIndex [아이템 컬럼 위치] \
-algorithmOption [속성간 유사도를 계산하기 위한 기본 알고리즘 지정 {jaccard, dice}] \
-sumOption [속성간 유사도를 합산하여 아이템간 유사도를 계산할때 합산할 방법 {avg, sum, cfsum}]

수행 예제:
hadoop jar ankus-core2-contents-sim-1.1.0.jar ContentBasedSimilarity -input /data/contents_sim.csv -output /result/contents_sim -delimiter , -subDelimiter '|' -indexList 1 -keyIndex 0 -algorithmOption jaccard -sumOption avg


*입력 예제
1,Toy Story (1995),Adventure|Animation|Children|Comedy|Fantasy
2,Jumanji (1995),Adventure|Children|Fantasy
3,Grumpier Old Men (1995),Comedy|Romance
4,Waiting to Exhale (1995),Comedy|Drama|Romance
5,Father of the Bride Part II (1995),Comedy
6,Heat (1995),Action|Crime|Thriller
7,Sabrina (1995),Comedy|Romance
8,Tom and Huck (1995),Adventure|Children
9,Sudden Death (1995),Action
10,GoldenEye (1995),Action|Adventure|Thriller
11,"American President, The (1995)",Comedy|Drama|Romance
12,Dracula: Dead and Loving It (1995),Comedy|Horror
13,Balto (1995),Adventure|Animation|Children
14,Nixon (1995),Drama
15,Cutthroat Island (1995),Action|Adventure|Romance
16,Casino (1995),Crime|Drama
17,Sense and Sensibility (1995),Drama|Romance
18,Four Rooms (1995),Comedy
19,Ace Ventura: When Nature Calls (1995),Comedy
20,Money Train (1995),Action|Comedy|Crime|Drama|Thriller
21,Get Shorty (1995),Comedy|Crime|Thriller
22,Copycat (1995),Crime|Drama|Horror|Mystery|Thriller
23,Assassins (1995),Action|Crime|Thriller
....
*출력 예제
**/contents_sim/part-r-00000
....
1,100034,0.5,1.0
1,100038,0.538,1.0
1,100042,0.773,1.0
1,100046,0.269,1.0
1,100050,1.286,1.0
1,100054,0.591,1.0
1,100058,0.686,1.0
1,100062,0.857,1.0
1,100070,0.464,1.0
1,100075,0.45,1.0
1,100079,0.778,1.0
1,100083,0.409,1.0
1,100087,1.095,1.0
1,100091,0.407,1.0
1,100096,1.136,1.0
1,1001,0.13,1.0
1,100101,0.308,1.0
1,100106,0.692,1.0
1,100131,0.667,1.0
1,100141,0.417,1.0
1,100145,0.682,1.0
1,100150,0.333,1.0
1,100157,0.474,1.0
....
