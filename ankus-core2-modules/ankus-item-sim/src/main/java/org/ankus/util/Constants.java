/*
 * Copyright (C) 2011 ankus (http://www.openankus.org).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.ankus.util;

	/**
	 * 알고리즘 수행 인자에 대한 상수를 정의함.
	 * @version 0.0.1
	 * @date : 2013.07.15
	 * @author Suhyun Jeon
	 * @author Moonie Song
	 */
	public class Constants {
	/**
	 * <font face="verdana" color="green">아이템 기반 유사도 추정 드라이버 호출명</font>
	 */
    public static final String DRIVER_ITEM_BASED_SIMILARITY = "ItemBasedSimilarity";
    
    /**
     * <font face="verdana" color="green">유사도 측정에 사용할 유클리스 거리 이름 상수</font>
     */
    public static final String CORR_UCLIDEAN = "uclidean";
    /**
     * <font face="verdana" color="green">유사도 측정에 사용할 코사인 유사도 이름 상수</font>
     */
    public static final String CORR_COSINE = "cosine";
    /**
     * <font face="verdana" color="green">유사도 측정에 사용할 피어슨 상관 계수 이름 상수</font>
     */
    public static final String CORR_PEARSON = "pearson";
   
}
