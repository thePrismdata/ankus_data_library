수치 범주형 데이터에 대해 유클리드/맨허튼 거리 측정 방법을 이용하여 상호간의 유사성을 추정하는 알고리즘.

수행 방법:
hadoop jar ankus-core2-numeric-similarity-1.1.0.jar \
NumericDataCorrelation \
-input [입력 데이터 파일 경로] \
-output [출력 데이터 폴더 경로] \
-keyIndex [대상 인덱스 리스트] \
-delimiter [구분자] \
-algorithmOption [유사도 계산 방법 {cosine|pearson|tanimoto|manhattan|uclidean}]

출력 데이터 폴더 경로 하위에 part-r-00000가 자동 생성됨

수행 예제 :
hadoop jar ankus-core2-numeric-similarity-1.1.0.jar \
NumericDataCorrelation \
-input /data/numeric_sim.csv \
-output /result/numeric_similarity \
-delimiter , \
-keyIndex 0 \
-algorithmOption cosine

*입력 예제
u,5,3,1,2,1,4,5,3
w,1,3,4,4,5,2,1,2

*출력 예제
**/result/numeric_similarity/part-r-00000 
u	w	0.605